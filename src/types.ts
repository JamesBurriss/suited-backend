export type Job = {
  ID: string;
  title: string;
  company: string;
  logo: string;
};

export type JobDescription = {
  desired: {
    personalityTraits: TraitAndEvidence[];
    technicalTraits: TraitAndEvidence[];
    educationalTraits: TraitAndEvidence[];
  };
  raw: string;
};

type TraitAndEvidence = {
  trait: Trait;
  evidence: Evidence[];
};

type Trait = {
  mainName: string;
  otherNames: string[];
  frameworks: string[];
  recruiterUsed: string[];
};

export type Evidence = {
  title: string;
  shortDescription: string;
  longDescription: string;
  thumbnail: string;
};

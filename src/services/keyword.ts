import * as glassdoorService from './glassdoor';

const stopword = require('stopword');

export async function extractKeywordsFromJob(ID: string) {
  const description: string = await glassdoorService.getJobDescriptionFromGlassdoor(ID);
  const words = splitWords(description);
  const cleanedText = words.join(' ');

  return ID;
}

function splitWords(text: string) {
  const SEPARATORS = /--+|_+| +|[|()[]\n]/;
  const ILLEGAL_WORD = /([^ ]+[.,\/#!$%\^&\*;:{}=\`~"“”‘’�][^ ]+|\d)/;
  const PUNCTUATION = /([^A-Za-z\u00C0-\u017F'-]|^'|^-)/g;

  const tokens = stopword.removeStopwords(
    text.split(SEPARATORS),
  );

  const words = tokens
    .filter(w => !ILLEGAL_WORD.test(w))   // Discard words containing punc
    .map(w => w.replace(PUNCTUATION, '')) // Remove surrounding punctuation
    .filter((w) => {
      return w.replace(/['-]/g, '')
              .length >= 2;
    }); // Discard words (without any special characters) below min length

  return words;
}

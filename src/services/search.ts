import * as glassdoorService from './glassdoor';
import config from '../config';

export async function search(searchTerm: string) {
  if (!searchTerm || searchTerm.length < 2) {
    return [];
  }

  const jobs = await glassdoorService.getJobsFromGlassdoor(searchTerm);
  const queriedJobs = jobs.filter(({ title, company }) => {
    const [titleLower, companyLower, searchTermLower] = [title, company, searchTerm]
                                                          .map(s => s.toLowerCase());

    return titleLower.includes(searchTermLower) || companyLower.includes(searchTermLower);
  });

  return queriedJobs.slice(0, config.maxResultsSize);
}

import { Job, JobDescription } from '../types';
import axios from 'axios';
import getJobDescription from '../mock/getJobDescription';
const cheerio = require('cheerio');

export async function getJobsFromGlassdoor(searchTerm: string): Promise<Job[]> {
  const { data } = await axios.get(
    `https://glassdoor.co.uk/Job/jobs.htm?sc.keyword=${searchTerm}`,
  );

  const $ = cheerio.load(data);
  const $jobListings = $('ul.jlGrid > li');
  const jobs = [];

  $jobListings.each(function () {
    const ID = $(this).attr('data-id');
    const title = $(this).find('div:last-child a.jobLink').text();
    const company = $(this).find('.empLoc > div').children()[0].prev.data;
    const logo = $(this).find('.logoWrap .sqLogo img').attr('data-original-2x');

    jobs.push({
      ID,
      title,
      logo,
      company: company.match(/[^ ]+/)[0],
    });
  });

  return jobs;
}

export async function getJobDescriptionFromGlassdoor(ID: string): Promise<string> {
  // const { data } = await axios.get(
  //   `https://www.glassdoor.co.uk/Job/json/details.htm?jobListingId=${ID}`,
  // );

  // const job = data.job;
  // const description = job.description;

  return getJobDescription.raw;

  // return description;
}

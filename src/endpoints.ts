import Router from 'express-promise-router';
import * as searchService from './services/search';
import * as keywordService from './services/keyword';
import config from './config';
import searchForAJob from './mock/searchForAJob';
import getJobDescription from './mock/getJobDescription';

const api = Router();

api.get('/job-search', async (req, res) => {
  if (config.shouldMockRequests) { return res.json(searchForAJob); }

  const { searchTerm } = req.query;
  const queriedCompanies = await searchService.search(searchTerm);

  return res.json(queriedCompanies);
});

api.get('/job-search/:ID', async (req, res) => {
  if (config.shouldMockRequests) { return res.json(getJobDescription); }

  const { ID } = req.params;
  const jobDescription = await keywordService.extractKeywordsFromJob(ID);

  return res.json(jobDescription);
});

export default api;

export default [
  {
    ID: '2992170103',
    title: 'Abuse Analyst',
    logo: 'https://media.glassdoor.com/sqlm/40772/facebook-squarelogo-1381810479272.png',
    company: 'Facebook',
  },
  {
    ID: '2992181384',
    title: 'Software Engineer, Core Data',
    logo: 'https://media.glassdoor.com/sqlm/40772/facebook-squarelogo-1381810479272.png',
    company: 'Facebook',
  },
  {
    ID: '2992175029',
    title: 'Product Manager',
    logo: 'https://media.glassdoor.com/sqlm/40772/facebook-squarelogo-1381810479272.png',
    company: 'Facebook',
  },
  {
    ID: '2992178975',
    title: 'Software Engineer, Intern/Co-op',
    logo: 'https://media.glassdoor.com/sqlm/40772/facebook-squarelogo-1381810479272.png',
    company: 'Facebook',
  },
  {
    ID: '2992176774',
    title: 'Content Strategy Internship',
    logo: 'https://media.glassdoor.com/sqlm/40772/facebook-squarelogo-1381810479272.png',
    company: 'Facebook',
  },
];

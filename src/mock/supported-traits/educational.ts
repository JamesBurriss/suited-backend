// tslint:disable max-line-length

import { Evidence } from '../../types';

const evidence: { [key: string]: Evidence } = {
  TrainRouteFinder: {
    title: 'Train Router Finder',
    shortDescription: `
      In 'Object Orientated Programming', a module taken in first year, I recieved 100% on a Java development coursework.
    `,
    longDescription: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec tortor vitae sem
      posuere pulvinar. Nam venenatis facilisis lorem sed bibendum. Ut id sem vel nunc
      sagittis sagittis eget at nulla. Suspendisse imperdiet velit in nisl aliquet,
      ut ultrices dolor aliquam. Vestibulum non turpis ultrices, malesuada justo eu,
      pellentesque neque. Duis eu lacus fringilla, aliquam ex nec, blandit orci. Duis nec
      bibendum diam, et aliquam lorem. Aenean ornare nisi eu est tincidunt, sed semper
      odio ullamcorper. Nullam a gravida ipsum, non tempor augue. Nulla nec elementum
      quam, quis eleifend felis. Mauris id eros elit.
    `,
    thumbnail: 'https://via.placeholder.com/256',
  },
  LoughboroughUniversity: {
    title: 'Studying Computer Science at Loughborough University',
    shortDescription: '',
    longDescription: '',
    thumbnail: 'https://via.placeholder.com/256',
  },
};

export default [
  {
    trait: {
      mainName: 'Bachelors Degree',
      regex: /(bachelors|bsc ?(hons)|masters|degree|first-class|GPA)/gi,
      recruiterUsed: 'First-class Degree',
    },
    evidence: [
      evidence.LoughboroughUniversity,
      evidence.TrainRouteFinder,
    ],
  },
];

// tslint:disable max-line-length

import { Evidence } from '../../types';

const evidence: { [key: string]: Evidence } = {
  MusicApplication: {
    title: 'Music Application',
    shortDescription: 'For my final year project, I\'m currently making a music application.',
    longDescription: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec tortor vitae sem
      posuere pulvinar. Nam venenatis facilisis lorem sed bibendum. Ut id sem vel nunc
      sagittis sagittis eget at nulla. Suspendisse imperdiet velit in nisl aliquet,
      ut ultrices dolor aliquam. Vestibulum non turpis ultrices, malesuada justo eu,
      pellentesque neque. Duis eu lacus fringilla, aliquam ex nec, blandit orci. Duis nec
      bibendum diam, et aliquam lorem. Aenean ornare nisi eu est tincidunt, sed semper
      odio ullamcorper. Nullam a gravida ipsum, non tempor augue. Nulla nec elementum
      quam, quis eleifend felis. Mauris id eros elit.
    `,
    thumbnail: 'https://via.placeholder.com/256',
  },
  HQTrivia: {
    title: 'HQ Trivia',
    shortDescription: 'In Summer 2018, I was working on HQ Trivia\'s API service.',
    longDescription: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec tortor vitae sem
      posuere pulvinar. Nam venenatis facilisis lorem sed bibendum. Ut id sem vel nunc
      sagittis sagittis eget at nulla. Suspendisse imperdiet velit in nisl aliquet,
      ut ultrices dolor aliquam. Vestibulum non turpis ultrices, malesuada justo eu,
      pellentesque neque. Duis eu lacus fringilla, aliquam ex nec, blandit orci. Duis nec
      bibendum diam, et aliquam lorem. Aenean ornare nisi eu est tincidunt, sed semper
      odio ullamcorper. Nullam a gravida ipsum, non tempor augue. Nulla nec elementum
      quam, quis eleifend felis. Mauris id eros elit.
    `,
    thumbnail: 'https://via.placeholder.com/256',
  },
  BlimeyCreative: {
    title: 'Blimey Creative',
    shortDescription: `
      I\'ve done two summer internships with creative agencies to develop
      my front-end and back-end web development skills
    `,
    longDescription: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec tortor vitae sem
      posuere pulvinar. Nam venenatis facilisis lorem sed bibendum. Ut id sem vel nunc
      sagittis sagittis eget at nulla. Suspendisse imperdiet velit in nisl aliquet,
      ut ultrices dolor aliquam. Vestibulum non turpis ultrices, malesuada justo eu,
      pellentesque neque. Duis eu lacus fringilla, aliquam ex nec, blandit orci. Duis nec
      bibendum diam, et aliquam lorem. Aenean ornare nisi eu est tincidunt, sed semper
      odio ullamcorper. Nullam a gravida ipsum, non tempor augue. Nulla nec elementum
      quam, quis eleifend felis. Mauris id eros elit.
    `,
    thumbnail: 'https://via.placeholder.com/256',
  },
  SeatingPlanner: {
    title: 'Seating Planner (PaperDraft)',
    shortDescription: `
      A seating planner application for high schools, written in PHP and JavaScript.
    `,
    longDescription: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec tortor vitae sem
      posuere pulvinar. Nam venenatis facilisis lorem sed bibendum. Ut id sem vel nunc
      sagittis sagittis eget at nulla. Suspendisse imperdiet velit in nisl aliquet,
      ut ultrices dolor aliquam. Vestibulum non turpis ultrices, malesuada justo eu,
      pellentesque neque. Duis eu lacus fringilla, aliquam ex nec, blandit orci. Duis nec
      bibendum diam, et aliquam lorem. Aenean ornare nisi eu est tincidunt, sed semper
      odio ullamcorper. Nullam a gravida ipsum, non tempor augue. Nulla nec elementum
      quam, quis eleifend felis. Mauris id eros elit.
    `,
    thumbnail: 'https://via.placeholder.com/256',
  },
  OneKudos: {
    title: 'One Kudos',
    shortDescription: `
      Between 2014 and 2016 I was a sole trader, running a server administration business.
    `,
    longDescription: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec tortor vitae sem
      posuere pulvinar. Nam venenatis facilisis lorem sed bibendum. Ut id sem vel nunc
      sagittis sagittis eget at nulla. Suspendisse imperdiet velit in nisl aliquet,
      ut ultrices dolor aliquam. Vestibulum non turpis ultrices, malesuada justo eu,
      pellentesque neque. Duis eu lacus fringilla, aliquam ex nec, blandit orci. Duis nec
      bibendum diam, et aliquam lorem. Aenean ornare nisi eu est tincidunt, sed semper
      odio ullamcorper. Nullam a gravida ipsum, non tempor augue. Nulla nec elementum
      quam, quis eleifend felis. Mauris id eros elit.
    `,
    thumbnail: 'https://via.placeholder.com/256',
  },
  TrainRouteFinder: {
    title: 'Train Router Finder',
    shortDescription: `
      In 'Object Orientated Programming', a module taken in first year, I recieved 100% on a Java development coursework.
    `,
    longDescription: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec tortor vitae sem
      posuere pulvinar. Nam venenatis facilisis lorem sed bibendum. Ut id sem vel nunc
      sagittis sagittis eget at nulla. Suspendisse imperdiet velit in nisl aliquet,
      ut ultrices dolor aliquam. Vestibulum non turpis ultrices, malesuada justo eu,
      pellentesque neque. Duis eu lacus fringilla, aliquam ex nec, blandit orci. Duis nec
      bibendum diam, et aliquam lorem. Aenean ornare nisi eu est tincidunt, sed semper
      odio ullamcorper. Nullam a gravida ipsum, non tempor augue. Nulla nec elementum
      quam, quis eleifend felis. Mauris id eros elit.
    `,
    thumbnail: 'https://via.placeholder.com/256',
  },
};

export default [
  {
    trait: {
      mainName: 'React',
      otherNames: ['React.js'],
      regex: /(react)/gi,
      frameworks: [],
      explanation: 'This year, I\'m currently developing a music application which is written in the ReactJS framework',
      recruiterUsed: 'ReactJS',
    },
    evidence: [
      evidence.MusicApplication,
    ],
  },
  {
    trait: {
      mainName: 'JavaScript',
      regex: /(JavaScript|\.JS|ES6|React|Angular|Vue\.?JS)/gi,
      otherNames: ['JS', 'ES6'],
      frameworks: ['React', 'Angular', 'Vue.js'],
      explanation: 'JavaScript is my strongest programming language. The majority of my past work have involved JavaScript, including working at different companies like HQ Trivia and Blimey Creative.',
      recruiterUsed: 'JavaScript',
    },
    evidence: [
      evidence.MusicApplication,
      evidence.HQTrivia,
      evidence.BlimeyCreative,
      evidence.SeatingPlanner,
    ],
  },
  {
    trait: {
      mainName: 'PHP',
      regex: /(PHP)/g,
      othernames: [],
      frameworks: ['Laravel', 'Symfony'],
      explanation: 'One of my personal projects, PaperDraft, was designed to be a seating planner for school. It featured a full, open-source OAuth 2.0 API to serve the frontend.',
      recruiterUsed: 'PHP',
    },
    evidence: [
      evidence.BlimeyCreative,
      evidence.SeatingPlanner,
    ],
  },
  {
    trait: {
      mainName: 'SASS',
      regex: /(SASS|LESS|SCSS)/gi,
      otherNames: ['LESS', 'SCSS'],
      frameworks: ['LESS', 'CSS'],
      explanation: 'SASS/LESS offers crucial benefits over CSS. It\'s a must. You\'ll find that all of my frontend projects make use of this.',
      recruiterUsed: 'SASS/LESS',
    },
    evidence: [
      evidence.MusicApplication,
      evidence.HQTrivia,
      evidence.BlimeyCreative,
      evidence.SeatingPlanner,
    ],
  },
  {
    trait: {
      mainName: 'Java',
      regex: /(Java[^S])/gi,
      otherNames: [],
      frameworks: ['Dagger', 'Angular'],
      explanation: 'Java was one of the earliest languages that I started with. It was crucial when I was developing plugins for my own business and it is used throughout my university coursework.',
      recruiterUsed: 'Java',
    },
    evidence: [
      evidence.OneKudos,
      evidence.TrainRouteFinder,
    ],
  },
  {
    trait: {
      mainName: 'TechOps',
      regex: /(TechOps)/gi,
      otherNames: ['DevOps', 'SysAdmin', 'Remote server administration', 'Linux', 'Servers'],
      frameworks: ['Redis'],
      explanation: 'I\'ve gained experience with TechOps through...',
      recruiterUsed: 'TechOps',
    },
    evidence: [
      evidence.OneKudos,
      evidence.HQTrivia,
    ],
  },
];

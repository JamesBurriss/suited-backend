// tslint:disable max-line-length

import { Evidence } from '../../types';

const evidence: { [key: string]: Evidence } = {
  LondonMarathon: {
    title: 'London Marathon 2018',
    shortDescription: `
      Raised over £1,900 by running in the London Marathon for Whizz-Kidz, a
      fantastic charity working hard to transform the lives of disabled children
      across the UK, supporting them to become confident and independent young adults.
    `,
    longDescription: `
      Raised over £1,900 by running in the London Marathon for Whizz-Kidz, a
      fantastic charity working hard to transform the lives of disabled children
      across the UK, supporting them to become confident and independent young adults.
    `,
    thumbnail: 'https://via.placeholder.com/256',
  },
};

export default [
  {
    trait: {
      mainName: 'Volunteering',
      regex: /(volunteer|hard[-?]working|motivated)/gi,
      recruiterUsed: 'Hard Working',
    },
    evidence: [
      evidence.LondonMarathon,
    ],
  },
];

import educationalTraits from './supported-traits/educational';
import personalityTraits from './supported-traits/personality';
import technicalTraits from './supported-traits/technical';

export default {
  desired: {
    // Use this to show education, courses taken, etc.
    educationalTraits,

    // Use this to show volunteering, hobbies, personal skills
    personalityTraits,

    // Use this to show projects built using this technology
    technicalTraits,
  },
  raw: `
    Facebook's mission is to give people the power to build community and bring
    the world closer together. Through our family of apps and services, we're
    building a different kind of company that connects billions of people around
    the world, gives them ways to share what matters most to them, and helps
    bring people closer together. Whether we're creating new products or helping
    a small business expand its reach, people at Facebook are builders at heart.
    Our global teams are constantly iterating, solving problems, and working
    together to empower people around the world to build community and connect
    in meaningful ways. Together, we can help people build stronger communities
    - we're just getting started.Want to build new features and products that
    touch more than a billion people around the world? Want to build new features
    that improve existing products like Photos, , Groups, NewsFeed, Search,
    and Messaging? Want to solve unique, large-scale, highly complex technical problems?

    Our development cycle is extremely fast, and we've built tools to keep it
    that way. It's common to write code and have it running live on the site
    just a few days later. If you work for us, you will make an impact, immediately.

    Facebook is seeking interns/co-ops to join our engineering team. You can help
    build the next generation of systems behind Facebook's products, create web
    applications that reach millions of people, build high volume servers and be a
    part of a team thats working to help connect people around the globe. This
    /co-op has a minimum twelve (12) week duration.

    Responsibilities:

        Code high-volume software using primarily C++ and Java
        Create web applications using primarily PHP
        Implement web interfaces using XHTML, CSS, and
        Build report interfaces and data feeds

    Mininum Qualifications:

        Pursuing a degree (Bachelors or Masters) in Computer Science or a related field
        Must be currently enrolled in a full-time degree program and returning to the
        program after the completion of the
        Experience in C++, Java, Perl, PHP, or
        High levels of creativity and quick problem solving capabilities
        Preferred: Demonstrated software engineering experience from previous , work
        experience, coding competitions, or publications
        Ability to obtain and maintain work authorization in the country of employment in 2018

    Preferred Qualifications:
  `,
};

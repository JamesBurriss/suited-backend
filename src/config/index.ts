export default {
  serverProtocol: 'https',
  serverAddress: '127.0.0.1',
  serverPort: 8080,
  shouldMockRequests: true,
  maxResultsSize: 5,
};

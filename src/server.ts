import endpoints from './endpoints';

const express = require('express');
const cors = require('cors');

const PORT = 8080;
const HOST = '0.0.0.0';
const app = express();

app.use(cors());
app.use('/v1/', endpoints);

app.listen(PORT, HOST);
console.log(`Running server on http://${HOST}:${PORT}`);

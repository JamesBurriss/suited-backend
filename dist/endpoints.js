"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_promise_router_1 = require("express-promise-router");
const searchService = require("./services/search");
const keywordService = require("./services/keyword");
const config_1 = require("./config");
const searchForAJob_1 = require("./mock/searchForAJob");
const getJobDescription_1 = require("./mock/getJobDescription");
const api = express_promise_router_1.default();
api.get('/job-search', (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (config_1.default.shouldMockRequests) {
        return res.json(searchForAJob_1.default);
    }
    const { searchTerm } = req.query;
    const queriedCompanies = yield searchService.search(searchTerm);
    return res.json(queriedCompanies);
}));
api.get('/job-search/:ID', (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (config_1.default.shouldMockRequests) {
        return res.json(getJobDescription_1.default);
    }
    const { ID } = req.params;
    const jobDescription = yield keywordService.extractKeywordsFromJob(ID);
    return res.json(jobDescription);
}));
exports.default = api;
